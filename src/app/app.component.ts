import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  activeMenu = 'infor';

  scrollPoint1(el: HTMLElement) {
    const infoMenu = document.getElementById('infoMenu')!,
      itemMenu = document.getElementById('itemMenu')!;

    infoMenu.classList.add("left-menu--underline");
    itemMenu.classList.remove("left-menu--underline");

    el.scrollIntoView();
  }

  scrollPoint2() {
    const infoMenu = document.getElementById('infoMenu')!,
      itemMenu = document.getElementById('itemMenu')!;

    itemMenu.classList.add("left-menu--underline");
    infoMenu.classList.remove("left-menu--underline");

    const p2 = document.getElementById('point_2')!;
    p2.scrollIntoView();
  }

  activeTab = 'search';

  search(activeTab: string) {
    this.activeTab = activeTab;
  }

  result(activeTab: string) {
    this.activeTab = activeTab;
  }

  menuCode = "";
  getMenuCode(val: string) {
    this.menuCode = val;
  }

  menuName = "";
  getMenuName(val: string) {
    this.menuName = val;
  }

  checkDates(group: FormGroup) {
    console.log(group.controls['endDate'].value);

    if (group.controls['endDate'].value < group.controls['startDate'].value) {
      return { notValid: true }
    }
    return null;
  }

  menuForm = new FormGroup({})

  constructor(private formBuilder: FormBuilder) {
    this.menuForm = this.formBuilder.group({
      menuCode: new FormControl('', [Validators.required]),
      menuName: new FormControl('', [Validators.required]),
      startDate: [''],
      endDate: ['']
    }, { validator: this.checkDates });
  }

  submitMenu() {
    console.warn(this.menuForm.value);
  }

  get getMenuCodeForm() {
    return this.menuForm.get("menuCode");
  }

  get getMenuNameForm() {
    return this.menuForm.get("menuName");
  }
}
